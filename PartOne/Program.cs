﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace PartOne
{
  class Program
  {
    static List<Thread> workers = new List<Thread>();
    static string[] stopwords = File.ReadAllText("../stop_words.txt").ToLower().Split(',');
    static Dictionary<string, int> word_freqs = new Dictionary<string, int>();
    static BlockingCollection<string> word_space = new BlockingCollection<string>();
    static BlockingCollection<Dictionary<string, int>> freq_space = new BlockingCollection<Dictionary<string, int>>();

    static void Main(string[] args)
    {
      var word = Regex.Split(File.ReadAllText(args[0]).ToLower(), "[\\W_]+");

      Console.WriteLine("populating word_space . . .");
      foreach(var w in word) word_space.Add(w);

      Console.WriteLine("starting threads (5)");
      for (var i = 0; i < 5; ++i)
      {
        var thd = new Thread(process_words);
        thd.Name = $"Thread {i}";
        workers.Add(thd);
      }
      foreach(var thd in workers) thd.Start();
      foreach(var thd in workers) thd.Join();

      while (!freq_space.Any())
      {
        var freqs = freq_space.Take();
        foreach (var kv in freqs)
        {
          var count = 0;
          if (freqs.ContainsKey(kv.Key))
          {
            count += freqs.Where(o => o.Key == kv.Key).Sum(o => o.Value);
            count += word_freqs.Where(o => o.Key == kv.Key).Sum(o => o.Value);
          }
          else
          {
            count = freqs[kv.Key];
          }
          word_freqs[kv.Key] = count;
        }
      }


      foreach (var itm in word_freqs.ToArray()
        .OrderByDescending(o => o.Value)
        .Take(25))
      {
        Console.WriteLine($"{itm.Key}  -  {itm.Value}");
      }

    }

    static void process_words()
    {
      Console.WriteLine($"Thread {Thread.CurrentThread.Name} executing...");
      while (true)
      {
        var word = "";
        if(!word_space.TryTake(out word, 1))
            break;

        if (word.Length > 1 && !stopwords.Contains(word))
        {
          if (word_freqs.ContainsKey(word))
            word_freqs[word]++;
          else
            word_freqs.Add(word, 1);
        }

        freq_space.Add(word_freqs);
      }
    }

  }

}


